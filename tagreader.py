#!/usr/bin/env python

'''Audio file metatag title reader for cross network display of file details'''

__author__ = 'Chris Shenkan'
__copyright__ = 'Copyright 2018, Shenkan & Associates'
__version__ = '1.0.1'
__license__ = 'GPL'
__email__ = 'cshenkan@shenkan-associates.com'
__program__ = 'Tag Reader'

from mutagen.id3 import ID3, ID3NoHeaderError
from mutagen.id3 import ID3, TIT2, TALB, TPE1, 	TPE2, COMM, USLT, TCOM, TCON, TDRC
from mutagen.flac import FLAC
import mutagen
import os
import os.path
import sys
import argparse
import textwrap
import datetime
import logging

# Globals
DEFAULT_EXTENSIONS = ['flac']
EXTENSIONS = []
LOGFILE = os.path.dirname(os.path.realpath(__file__)) + '/logs/log_' + datetime.datetime.now().strftime('%Y_%m_%d_%H_%M') + '.txt'
LOG = False
__logger = None

class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter, argparse.RawDescriptionHelpFormatter):
    pass

class SourcePathException(Exception):
	def __init__(self, message):
		super().__init__(message)

def parser_setup():
	# set up command line argument parser
	parser = argparse.ArgumentParser(prog='Tag Reader', formatter_class=CustomFormatter,
		description=textwrap.dedent(f'''\
			{__program__} {__version__}
			By {__author__}
			{__email__}
			{__copyright__}
			{__license__}

			Used to display tags for audio files in a directory and go through them one by 
			one to decide whether or not to add an '>' at the end signifying it plays into 
			the next track
			'''))
	parser.add_argument('-s', '--source', help='directory to read files from', nargs='?', default='\\\\192.168.0.70\Request\grateful_dead')
	parser.add_argument('-V', '--version', help='display version information', action='version', version=f'{__program__} {__version__}')
	parser.add_argument('-v', '--verbose', help='increase output verbosity', action='store_true')
	parser.add_argument('-t', '--titles', help='print only titles', action='store_true')
	parser.add_argument('-o', '--output', help='file to store out to', type=argparse.FileType('w'), nargs='?', default=sys.stdout)
	parser.add_argument('-l', '--log', help='whether or not to log errors to file.', action='store_true')
	parser.add_argument('-e', '--extensions', help='file extensions to accept for processing', metavar='N', nargs='+', default=DEFAULT_EXTENSIONS)
	parser.add_argument('-u', '--user', help='username to access directory', default='SHENKAN\\sshenkan')
	parser.add_argument('-p', '--password', help='password to access directory', default='~!SMS@sa')
	args = parser.parse_args()

	return args

def logger_setup():
	logger = logging.getLogger(__name__)
	log_handler = logging.FileHandler(LOGFILE)
	log_handler.setLevel(logging.ERROR)
	log_formatter = logging.Formatter('%(asctime)s - %(levelname)s  -  %(message)s')
	log_handler.setFormatter(log_formatter)
	logger.addHandler(log_handler)
	return logger

class AudioFile:
	'''Object representing an audio file in the directory.'''

	def __init__(self, audio_file, path, album_dir, artist_dir, extensions=EXTENSIONS):
		self.file = process_file(audio_file)
		self.filename, self.file_extension = os.path.splitext(audio_file)
		self.path = path
		self.album_dir = album_dir
		self.artist_dir = artist_dir
		self.filetype = audio_file
		self.extensions = extensions
		
		self.title = self.file.get('title', self.title = self.filename[3:].replace('_', ' ').title())
		self.artist = self.file.get('artist', self.artist_dir.replace('_', ' ').title())
		self.album = self.file.get('album', self.album_dir.replace[10:].replace('_', ' ').title())
		self.year = self.file.get('year', self.album_dir[:4])
		self.track_no = self.file.get('tracknumber', self.filename[:2])
		self.date = self.file.get('date', self.album_dir[:10].replace('_', '-')[::-1])

	def __repr__(self):
		return f'AudioFile({self.file}, {self.title})'

	def __str__(self):
		return f'[AudioFile] {self.track_no}  {self.title}  -  {self.artist}  -  {self.album}\t{self.filename}'

	@staticmethod
	def is_accepted_filetype(f):
		if not os.isfile(f):
			if LOG:

				__logger.error(f'File {f} is not a file.  Terminating Execution.')
			return False
		name, extension = os.path.splitext(f)
		if extension not in self.extensions:
			if LOG:
				__logger.error(f'File {name}\'s extension, {extension} is not in list of accepted extensions {self.extensions}.  Terminating Execution.')
			return False
		else:
			return True	

	def process_file(self, audio_file):
		try:
			processed_file = mutagen.File(audio_file)
		except mutagen.MutagenError as e:
			if LOG:
				__logger.error(f'Error processing file {audio_file}  -  {str(e)} {e.message}  -  Skipping file.')
				print(f'Error processing file {audio_file}  -  {str(e)} {e.message}  -  Skipping file.')
		return(processed_file)

	def check_title(self):
		if self.title.rstrip()[-1] is '>':
			return True
		else:
			return False

	def compare_title(self):
		# Possible future implementation
		pass

	def add_arrow(self):
		self.title = self.title + ' >'

	def save_tags(self):
		self.file.save()


class AudioAlbum:
	'''Object representing an album which is a directory in the file system.'''

	def __init__(self, path, album_dir, artist_dir):
		self.path = path
		self.album_dir = album_dir
		self.artist_dir = artist_dir
		self.tracks = []
		self.num_tracks = 0
		self.title = self.album_dir.replace('_', ' ').title()[10:]
		self.artist = self.artist_dir.replace('_', ' ').title()

	def __repr__(self):
		return f'AudioAlbum({self.file}, {self.title})'

	def __str__(self):
		return f'[AudioAlbum] {self.title}  -  {self.artist}\t{self.file}'

	def add_track(self, track):
		tracks.add(track)
		self.num_tracks += 1

def connect(path, user, password):
	os.system(r'net use ' + path + ' /user:' + username + ' ' + password)

def process_directory(path):
	shows = []
	artist_dir = path.split('\\')[-1]
	for show in os.listdir(path):
		songs = []
		if os.path.isdir(show):		
			for song in os.listdir(d):
				if AudioFile.is_accepted_filetype(song):
					audio_file = AudioFile(song, path + '\\' + song, show, artist_dir)
					songs.add(audio_file)
			shows.add((show, songs))
	return shows

def check_source(path):
	if not os.path.isdir(path):
		raise SourcePathException(f'{path} is not a directory.\nTerminating Execution.')

def check_extensions(extensions):
	if extensions != DEFAULT_EXTENSIONS:
		EXTENSIONS = extensions
		i = 0
		for item in EXTENSIONS:
			if item.startswith('.'):
				EXTENSIONS[i] = item.strip('.')
			i += 1
	else:
		EXTENSIONS = DEFAULT_EXTENSIONS

def main():
	args = parser_setup()
	path, extensions, username, password = args.source, args.extensions, args.user, args.password

	LOG = args.log

	if LOG:
		__logger = logger_setup()

	check_extensions(extensions)
	
	try:	
		connect(path, username, password)
		
		check_source(path)
	except OSError as e:
		if LOG:
			__logger.error(f'[{e.errno}] {str(e)}  -  Excution of command failed using {username}/{password} with path {path} and args {e.args}  Terminating Execution.')
		sys.exit(f'[{e.errno}] {str(e)}  -  Excution of command failed using {username}/{password} with path {path} and args {e.args}\nTerminating Execution.')
	except SourcePathException as e:
		if LOG:
			__logger.error(f'{str(e)}  -  {e.message}')
		sys.exit(f'{str(e)}  -  {e.message}')
	except:
		if LOG:
			__logger.error('Encounted an unexpected error.  Terminating Execution.')
		sys.exit('Encounted an unexpected error.\nTerminating Execution.')

	shows = process_directory(path)


if __name__ == '__main__':
	main()